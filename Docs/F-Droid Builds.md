## [Brave Browser Build on F-Droid](#brave-browser-build-on-f-droid)

- [Offer support for LineageOS/F-Droid #7027 (github.com)](https://github.com/brave/brave-browser/issues/7027)
